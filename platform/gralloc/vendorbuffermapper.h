/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_PLATFORM_GRALLOC_VENDOR_BUFFER_MAPPER_H_
#define ANDROID_PLATFORM_GRALLOC_VENDOR_BUFFER_MAPPER_H_

#include "vendormapper.h"

class VendorGraphicBufferMapper {
 public:
  static VendorGraphicBufferMapper& getInstance() {
    static VendorGraphicBufferMapper gInstance;
    return gInstance;
  }

  ~VendorGraphicBufferMapper() = default;
  VendorGraphicBufferMapper(const VendorGraphicBufferMapper&) = delete;
  VendorGraphicBufferMapper& operator=(const VendorGraphicBufferMapper&) =
      delete;

  android::status_t getPlaneFds(buffer_handle_t bufferHandle,
                                std::vector<int32_t>* fds);
  android::status_t importBuffer(buffer_handle_t rawHandle,
                                 buffer_handle_t* outHandle);

 private:
  VendorGraphicBufferMapper();

  std::unique_ptr<const VendorMapper> mapper_;
};

#endif