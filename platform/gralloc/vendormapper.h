/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_PLATFORM_GRALLOC_VENDOR_MAPPER_H_
#define ANDROID_PLATFORM_GRALLOC_VENDOR_MAPPER_H_

#include <cutils/native_handle.h>
#include <utils/Errors.h>
#include <cstdint>
#include <vector>

class VendorMapper {
 public:
  virtual ~VendorMapper() = default;

  /*
   * Call before other functions to check that IMapper 4 service is available.
   */
  virtual bool isLoaded() const = 0;

  /*
   * Get the file descriptor for the buffer for each plane.
   * This is not part of the standard metadata so makes use of vendor defined
   * metadata type metadataType_plane_fds.
   */
  virtual android::status_t getPlaneFds(buffer_handle_t bufferHandle,
                                        std::vector<int32_t>* fds) const {
    (void)bufferHandle;
    (void)fds;
    return android::INVALID_OPERATION;
  }

  /*
   * Imports a raw buffer handle to create an imported buffer handle for use
   * with the rest of the mapper or with other in-process libraries.
   */
  virtual android::status_t importBuffer(buffer_handle_t rawHandle,
                                         buffer_handle_t* outHandle) const {
    (void)rawHandle;
    (void)outHandle;
    return android::INVALID_OPERATION;
  }
};

#endif