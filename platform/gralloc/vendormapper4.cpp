/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "platformgrallocmetadatatypes.h"
#include "vendormapper.h"

using android::hardware::graphics::mapper::V4_0::Error;

class VendorMapper4 : public VendorMapper {
 public:
  VendorMapper4() {
    mapper_ = IMapper::getService();
    if (mapper_ == nullptr) {
      ALOGI("mapper 4.x is not supported");
      return;
    }
    if (mapper_->isRemote()) {
      LOG_ALWAYS_FATAL("gralloc-mapper must be in passthrough mode");
    }
  }

  bool isLoaded() const {
    return mapper_ != nullptr;
  }

  virtual android::status_t importBuffer(
      buffer_handle_t rawHandle, buffer_handle_t* outHandle) const override {
    android::status_t result = android::OK;
    auto ret = mapper_->importBuffer(rawHandle, [&](const auto& error,
                                                    const auto& buffer) {
      if (error != Error::NONE) {
        result = android::BAD_VALUE;
        return;
      }
      *outHandle = static_cast<buffer_handle_t>(buffer);
    });

    return result;
  }

  virtual android::status_t getPlaneFds(
      buffer_handle_t bufferHandle, std::vector<int32_t>* fds) const override {
    android::status_t result = android::OK;

    const void* handle = reinterpret_cast<const void*>(bufferHandle);
    mapper_->get(const_cast<void*>(handle), android::metadataType_plane_fds,
                 [&result, fds](Error error,
                                const hidl_vec<uint8_t>& metadata) {
                   if (error != Error::NONE) {
                     result = android::BAD_VALUE;
                     return;
                   }
                   result = android::DecodePlaneFds(metadata, fds);
                 });
    if (result != android::OK) {
      return result;
    }
    if (fds->size() < 1) {
      return android::BAD_VALUE;
    }
    return android::OK;
  }

 private:
  android::sp<android::hardware::graphics::mapper::V4_0::IMapper> mapper_;
};

std::unique_ptr<const VendorMapper> CreateVendorMapper4() {
  return std::make_unique<const VendorMapper4>();
}
