/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "platformgrallocmetadatatypes.h"

namespace android {

status_t EncodePlaneFds(const std::vector<int32_t>& fds,
                        hidl_vec<uint8_t>* output) {
  int32_t n_fds = fds.size();

  output->resize((n_fds + 1) * sizeof(int32_t));

  memcpy(output->data(), &n_fds, sizeof(n_fds));
  memcpy(output->data() + sizeof(n_fds), fds.data(), sizeof(int32_t) * n_fds);

  return android::OK;
}

status_t DecodePlaneFds(const hidl_vec<uint8_t>& input,
                        std::vector<int32_t>* fds) {
  int32_t size = 0;

  memcpy(&size, input.data(), sizeof(int32_t));
  if (size < 0) {
    ALOGE("Bad fds size");
    return android::BAD_VALUE;
  }

  fds->resize(size);

  const uint8_t* tmp = input.data() + sizeof(int32_t);
  memcpy(fds->data(), tmp, sizeof(int32_t) * size);

  return android::OK;
}

}  // namespace android