/*
 * Copyright (C) 2020 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef PLATFORM_GRALLOC_METADATA_TYPES_H
#define PLATFORM_GRALLOC_METADATA_TYPES_H

#include <aidl/drm_hwcomposer/graphics/DrmHwcMetadataType.h>
#include <android/hardware/graphics/mapper/4.0/IMapper.h>
#include <log/log.h>
#include "gralloctypes/Gralloc4.h"

using aidl::drm_hwcomposer::graphics::DrmHwcMetadataType;
using android::hardware::graphics::mapper::V4_0::IMapper;
using android::hardware::hidl_vec;

namespace android {

#define DRM_HWCOMPOSER_METADATA_TYPE "drm_hwcomposer.graphics.metadataType"

static const IMapper::MetadataType metadataType_plane_fds =
    {DRM_HWCOMPOSER_METADATA_TYPE,
     static_cast<int64_t>(DrmHwcMetadataType::PLANE_FDS)};

status_t EncodePlaneFds(const std::vector<int32_t>& fds,
                        hidl_vec<uint8_t>* output);
status_t DecodePlaneFds(const hidl_vec<uint8_t>& input,
                        std::vector<int32_t>* fds);
}

#endif
